import requests
import numpy as np
import json
dim1=25
dim2=25
def read_file(path):
	f = open(path , "r")
	x = []
	count = 0
	for line in f:
		count += 1
		if count > 25000:
			break
		g = line.split(";")[0]
		i = line.split(";")[1]
		x.append((float(g) ))
		x.append((float(i) ))
	return np.array(x[10000:(10000+dim1*dim2*2)])
file_path="/home/tainv/Downloads/signal_25000_samples/wfm/txt/4_wfm_96050000_hz.txt"
signal=read_file(file_path)
print(signal)
headers = {
            'Content-Type': 'application/json',
}
print("Read data from ",file_path)
r = requests.post("http://127.0.0.1:5000/classification", headers=headers,data=json.dumps(signal.tolist()))
print ("Result from server :")
print(r.text)
